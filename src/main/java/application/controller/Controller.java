package application.controller;

import application.crawler.Crawler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class Controller {

    private Logger log = LoggerFactory.getLogger(Controller.class);

    @Autowired
    private Crawler crawler;

    @RequestMapping(value = "/trigger")
    public String trigger(){
        log.info("Crawler triggered manually");
        return crawler.run("https://www.2iqresearch.com/");
    }
}
