package application.crawler;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Crawler {

    private Logger log = LoggerFactory.getLogger(Crawler.class);

    private WebDriver webDriver;

    @PostConstruct
    private void init(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        webDriver = new ChromeDriver(options);
    }

    public String run(String url){
        log.info("Starting");

        String result;
        webDriver.get(url);

        try{
            WebDriverWait wait = new WebDriverWait(webDriver, 5);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.hs-field-desc")));
            result = webDriver.getPageSource();
            log.info("Finished successfully");
        }catch(Exception e){
            log.error("Failed", e);
            result = "Operation failed";
        }

        return result;
    }
}
