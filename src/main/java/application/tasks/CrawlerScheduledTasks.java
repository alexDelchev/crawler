package application.tasks;

import application.crawler.Crawler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CrawlerScheduledTasks {

    private Logger log = LoggerFactory.getLogger(CrawlerScheduledTasks.class);

    @Autowired
    Crawler crawler;

    @Scheduled(fixedDelay = 20000)
    private void runCrawler(){
        log.info("Crawler triggered automatically");
        crawler.run("https://www.2iqresearch.com/");
    }
}
